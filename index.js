// ex1
function tinhLuong(){
    var luongNgay = document.getElementById("luong-mot-ngay").value *1;
    var soNgay = document.getElementById("so-ngay-lam").value *1;
    var tienLuong = luongNgay * soNgay;

    document.getElementById("tinh-luong").innerText = `👉 ${tienLuong}`;
};


// ex2
function tinhTB(){
    var so1 = document.getElementById("so-1").value *1;
    var so2 = document.getElementById("so-2").value *1;
    var so3 = document.getElementById("so-3").value *1;
    var so4 = document.getElementById("so-4").value *1;
    var so5 = document.getElementById("so-5").value *1;

    var trungBinh = (so1 + so2 + so3 + so4 + so5) / 5;

    document.getElementById("tinh-tb").innerText =`👉 ${trungBinh}`;
};


// ex3
var USD = 23500;

function doiTien(){
    var soUSD = document.getElementById("so-usd").value *1;
    var soVND = USD * soUSD;

    document.getElementById("doi-tien").innerText = `👉 ${new Intl.NumberFormat('vn-VN', {style: 'currency', currency: 'VND'}).format(soVND)}`;
};


// ex4
function tinhDientichChuvi(){
    var chieuDai = document.getElementById("chieu-dai").value *1;
    var chieuRong = document.getElementById("chieu-rong").value *1;

    var dienTich = chieuDai * chieuRong;
    var chuVi = (chieuDai + chieuRong) *2;

    document.getElementById("tinh-dientich-chuvi").innerText = `👉 Diện tich: ${dienTich}, Chu vi: ${chuVi}`;
};

// ex5
function tinhkySo(){
    var n = document.getElementById("so-co-2-chu-so").value *1;

    var donvi = n % 10;
    var chuc = Math.floor(n/10);
    var tongkySo = donvi + chuc;

    document.getElementById("tinh-tong-2-ky-so").innerText = `👉 ${tongkySo}`;
};